// https://www.w3schools.com/java/java_iterator.asp
import java.util.Iterator;

public class DinerMenu implements Menu {
	static final int MAX_ITEMS = 6;
	int numberOfItems = 0;
	MenuItem[] menuItems;

	public DinerMenu() {
		menuItems = new MenuItem[MAX_ITEMS];

		addItem("vegan BLT",
				"whole wheat and beans, tomato",
				true,
				2.99);

		addItem("BLT",
				"whole wheat and bacon, cabagge, tomato",
				false,
				2.99);

		addItem("today's soup",
				"potato salad",
				false,
				3.29);

		addItem("hotdog",
				"sour craut, spices, onion, and cheese",
				false,
				3.05);
	}

	public void addItem(String name, String description,
						boolean vegetarian, double price)
	{
		MenuItem menuItem = new MenuItem(name, description, vegetarian, price);

		if (numberOfItems >= MAX_ITEMS) {
			System.err.println("item full");
		} else {
			menuItems[numberOfItems] = menuItem;
			numberOfItems = numberOfItems + 1;	
		}
	}

	/*
	public MenuItem[] getMenuItems() {
		return menuItems;
	}
	*/

//	public Iterator getMenuItems() {
	public Iterator<MenuItem> createIterator() {
		DinerMenuIterator iterator = new DinerMenuIterator(menuItems);
		return iterator;
	}

	// etc
}
