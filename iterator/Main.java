// https://www.w3schools.com/java/java_getstarted.asp
// - javac *.java
// - java Main

import java.util.List;
import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {
		List<Menu> menus = new ArrayList<Menu>();

		menus.add(new PancakeHouseMenu());
		menus.add(new DinerMenu());
		menus.add(new CafeMenu());

		Waitress waitress = new Waitress(menus);

		waitress.printMenu();
	}
}
