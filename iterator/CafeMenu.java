import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;

public class CafeMenu implements Menu {
	// 
	Map<String, MenuItem> menuItems = new HashMap<String, MenuItem>();
	
	public CafeMenu () {
		addItem("vegeterian burger and fries",
				"whole wheat, cabbage, tomato, and potato fries with vegetarian burger",
				true,
			    3.99);
		addItem("today's soup",
				"salad with today's soup",
				false,
				3.69);
		addItem("burito",
				"whole pinto beans and salsa, and guacamole with burito",
				true,
				4.29);
	}

	public void addItem(String name, String description,
						boolean vegetarian, double price)
	{
		MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
		menuItems.put(name, menuItem);
	}

	/*
	public Map<String, MenuItem> getMenuItems() {
		return menuItems;
	}
	*/

	public Iterator<MenuItem> createIterator() {
		// Returns a Collection view of the values contained in this map.
		// - Collection<V>
		return menuItems.values().iterator();
	}
}
