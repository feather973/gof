import java.util.Iterator;

// if Aggregate class (DinerMenu) does not return Iterator,
// We just create Iterator class for ourselves
public class DinerMenuIterator implements Iterator<MenuItem> {
	MenuItem[] items;
	int position = 0;

	public DinerMenuIterator(MenuItem[] items) {
		this.items = items;
	}

	public MenuItem next() {
		MenuItem item = items[position];
		position = position + 1;

		return item;
	}

	public boolean hasNext() {
		if (position >= items.length || items[position] == null)
			return false;
		else
			return true;
	}

	public void remove() {
		throw new UnsupportedOperationException("cannot delete menu");
	}
}
