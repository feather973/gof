// https://www.w3schools.com/java/java_arraylist.asp
import java.util.ArrayList;
import java.util.Iterator;
//import java.util.List;

public class PancakeHouseMenu implements Menu {
	//List<MenuItem> menuItems;		// menuItem's'
	ArrayList<MenuItem> menuItems;		// menuItem's'

	// "main"
	public PancakeHouseMenu() {
		menuItems = new ArrayList<MenuItem>();		// implementation of List

		addItem("K&B pancake set",
				"scramble egg and toast",
				true,
				2.99);

		addItem("regular pancake set",
				"fried egg and sausage",
				false,
				2.99);
		
		addItem("blueberry pancake",
				"blueberry and syrup",
				true,
				3.49);

		addItem("waffle",
				"blueberry and strawberry",
				true,
				3.59);
	}

	public void addItem(String name, String description,
						boolean vegetarian, double price)
	{
		MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
		menuItems.add(menuItem);
	}

	public Iterator<MenuItem> createIterator() {
		return menuItems.iterator();
	}

	/*
	// hmm, that return type..
	public ArrayList<MenuItem> getMenuItems() {
		return menuItems;
	}
	*/

	// etc
}
