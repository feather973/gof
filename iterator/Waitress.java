import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Waitress {
	List<Menu> menus;
	/*
	Menu pancakeHouseMenu;
	Menu dinerMenu;
	Menu cafeMenu;
	*/

	public Waitress(List<Menu> menus) {
		this.menus = menus;
	}

	public void printMenu() {
		// List also iterator() method
		Iterator<Menu> menuIterator = menus.iterator();

		while (menuIterator.hasNext()) {
			// no need to know aggregate class
			Menu menu = menuIterator.next();
			printMenu(menu.createIterator());
		}

		/*
		//Iterator breakfast = pancakeHouseMenu.getMenuItems();
		Iterator<MenuItem> breakfast = pancakeHouseMenu.createIterator();
		Iterator<MenuItem> lunch = dinerMenu.createIterator();
		Iterator<MenuItem> dinner = cafeMenu.createIterator();
		*/

		/*
		System.out.println("==== breakfast ====");
		printMenu(breakfast);

		System.out.println("====   lunch   ====");
		printMenu(lunch);

		System.out.println("====   dinner  ====");
		printMenu(dinner);
		*/
	}

// no need to know about aggregate classes (Menu's concrete class), good
//	public void printMenu(Iterator iterator) {
	public void printMenu(Iterator<MenuItem> iterator) {
		while (iterator.hasNext()) {
			MenuItem item = iterator.next();
			System.out.println(item.getName() + " ");
			System.out.println(item.getPrice() + " ");
			System.out.println(item.getDescription());
		}
	}

	/*
	public void printMenu() {
		// breakfast from PancakeHouseMenu
		// lunch from DinerMenu

		// oops.. 
		ArrayList<MenuItem> breakfast = pancakeHouseMenu.getMenuItems();
		MenuItem[] lunch = dinerMenu.getMenuItems();

		// https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html
		for (int i=0; i<breakfast.size(); i++) {
			MenuItem menuItem = breakfast.get(i);

			System.out.println(menuItem.getName() + " ");
			System.out.println(menuItem.getPrice() + " ");
			System.out.println(menuItem.getDescription());
		}
		
		// https://www.w3schools.com/java/java_arrays.asp
		// use length property (but, it will cause exception since array has uninitialized elemends)
		for (int i=0; i<lunch.length; i++) {
			MenuItem menuItem = lunch[i];

			System.out.println(menuItem.getName() + " ");
			System.out.println(menuItem.getPrice() + " ");
			System.out.println(menuItem.getDescription());
		}
	}
	*/
}
